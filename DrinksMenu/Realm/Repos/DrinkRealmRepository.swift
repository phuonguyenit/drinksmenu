//
//  DrinkRealmRepository.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation
import RxSwift
import Realm
import RealmSwift

protocol DrinkRealmRepoType {
    func queryAll() -> Observable<[DrinkItem]>
    func save(_ item: DrinkItem) -> Observable<Void>
}

final class DrinkRealmRepository<RealmRepository>: DrinkRealmRepoType where RealmRepository: RealmRepoAbstract, RealmRepository.T == DrinkItem {
    private let repository: RealmRepository
    
    init(repository: RealmRepository) {
        self.repository = repository
    }

    func queryAll() -> Observable<[DrinkItem]> {
        return self.repository.queryAll()
    }
    
    func save(_ item: DrinkItem) -> Observable<Void> {
        return repository.save(entity: item)
    }
}
