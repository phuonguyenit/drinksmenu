//
//  MockDataGenerator.swift
//  DrinksMenuTests
//
//  Created by phuonguyen on 05/08/2022.
//

import Foundation
@testable import DrinksMenu

class MockDataGenerator {
    static let shared = MockDataGenerator()
    
    func mockDrinkData() -> Drink {
        let items = mockDrinkItemsData()
        return Drink(items)
    }
    
    func mockDrinkItemsData() -> [DrinkItem] {
        let items: [DrinkItem] = (1...5).map {
            return DrinkItem(
                id: "id_\($0)",
                imageUrl: nil,
                name: "item name \($0)",
                instruction: "This is instruction of drink item \($0)"
            )
        }
        
        return items
    }
    
    func mockDrinkItem() -> DrinkItem {
        return DrinkItem(
            id: "id_test",
            imageUrl: nil,
            name: "item test name",
            instruction: "This is instruction of drink item test"
        )
    }
    
    func mockDrinkUseCase() -> DrinkUseCaseType {
        let drinkNetwork = DrinkNetworkMock()
        let drinkRealmRepository = DrinkRealmRepositoryMock()
        let drinkUseCase = DrinkUseCase(drinkNetworkService: drinkNetwork, drinkRealmRepo: drinkRealmRepository)
        
        return drinkUseCase
    }
}
