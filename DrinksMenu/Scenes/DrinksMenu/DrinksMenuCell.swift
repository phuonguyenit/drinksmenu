//
//  DrinksMenuCell.swift
//  DrinksMenu
//
//  Created by phuonguyen on 02/08/2022.
//

import UIKit
import SnapKit
import Kingfisher
import RxCocoa

final class DrinksMenuCell: UITableViewCell {
    
    private let CACHE_PREFIX = "cache_image-"
    
    private let containerView: UIView = {
       let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        return view
    }()
    
    private let contentImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.layer.cornerRadius = 10
        view.backgroundColor = .white
        return view
    }()
    
    private let drinkLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 20, weight: .bold)
        view.textColor = .black
        view.numberOfLines = 0
        return view
    }()
    
    private let instructionLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 18)
        view.textColor = .black
        view.numberOfLines = 0
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
        setupConstraints()
    }

    private func setupView() {
        containerView.addSubview(contentImageView)
        containerView.addSubview(drinkLabel)
        containerView.addSubview(instructionLabel)
        contentView.addSubview(containerView)
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    private func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(8)
            $0.leading.trailing.equalToSuperview().inset(16)
        }
        
        contentImageView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(16)
            $0.height.equalTo(contentImageView.snp.width).multipliedBy(1)
        }
        
        drinkLabel.snp.makeConstraints {
            $0.top.equalTo(contentImageView.snp.bottom).offset(16)
            $0.leading.trailing.equalToSuperview().inset(16)
        }
        
        instructionLabel.snp.makeConstraints {
            $0.top.equalTo(drinkLabel.snp.bottom).offset(16)
            $0.leading.trailing.equalToSuperview().inset(16)
            $0.bottom.equalToSuperview().inset(16)
        }
    }
    
    func bind(_ item: MenuItem) {
        setupImageView(item)
        drinkLabel.text = item.name
        instructionLabel.text = item.instruction
    }
    
    private func setupImageView(_ item: MenuItem) {
        guard let imageUrl = URL(string: item.imageURL ?? "") else { return }
        
        let resource = ImageResource(downloadURL: imageUrl, cacheKey: CACHE_PREFIX + item.id)
        contentImageView.kf.setImage(with: resource)
    }
}
