//
//  DrinksMenuViewModel.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation
import RxSwift
import RxCocoa

final class DrinksMenuViewModel {
    private let useCase: DrinkUseCaseType
    private let disposeBag = DisposeBag()
    
    init(useCase: DrinkUseCaseType) {
        self.useCase = useCase
    }
    
    private func loadDrinks(parameters: OutputParameters){
        useCase.fetchDrinksFromNetwork()
            .trackActivity(parameters.activityIndicator)
            .trackError(parameters.errorTracker)
            .do(onNext: { drink in
                self.saveListDrinkItems(drink.items)
            })
            .asDriver(onErrorDriveWith: useCase.fetchDrinksFromDatabase().asDriverOnErrorJustComplete().map({ items in
                return Drink(items)
            }))
            .map {
                return $0.items.map { return MenuItem($0) }
            }
            .drive(parameters.outDrinks)
            .disposed(by: disposeBag)
    }
    
    private func saveListDrinkItems(_ items: [DrinkItem]) {
        items.forEach {
            saveDrinkItem($0)
        }
    }
    
    private func saveDrinkItem(_ item: DrinkItem) {
        _ = useCase.saveDrinkItemToDatabase(item)
            .asDriverOnErrorJustComplete()
            .drive()
            .disposed(by: disposeBag)
    }
}

extension DrinksMenuViewModel: ViewModelType {
    struct Input {
        let loadDrinksTrigger: Driver<Void>
    }
    
    struct Output {
        let outDrinks: Driver<[MenuItem]>
        let isLoading: Driver<Bool>
        let error: Driver<Error>
    }
    
    private struct OutputParameters {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        let outDrinks = BehaviorRelay<[MenuItem]>(value: [])
    }
    
    func transform(input: Input) -> Output {
        let parameters = OutputParameters()
        input.loadDrinksTrigger
            .drive(onNext: { [weak self] in
                self?.loadDrinks(parameters: parameters)
            })
            .disposed(by: disposeBag)
        
        return Output(outDrinks: parameters.outDrinks.asDriver(),
                      isLoading: parameters.activityIndicator.asDriver(),
                      error: parameters.errorTracker.asDriver())
    }
}


