//
//  DrinkRequest.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation

final class DrinkRequest: BaseRequestProtocol {
    typealias ResponseType = Drink
    var path: String = "/v1/1/search.php?s=margarita"
}
