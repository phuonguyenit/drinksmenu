//
//  DrinkNetwork.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation
import RxSwift

protocol DrinkNetworkType {
    func fetchDrinks() -> Single<Drink>
}

final class DrinkNetwork: DrinkNetworkType {
    private let network: Network
    
    init(network: Network) {
        self.network = network
    }
    
    func fetchDrinks() -> Single<Drink> {
        return network.excute(DrinkRequest())
    }
}
