//
//  DrinksMenuModel.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation

struct MenuItem {
    var id: String
    var imageURL: String?
    var name: String?
    var instruction: String?
    
    init(_ source: DrinkItem) {
        self.id = source.id
        self.imageURL = source.imageUrl
        self.name = source.name
        self.instruction = source.instruction
    }
    
    func toDrinkItem() -> DrinkItem {
        return DrinkItem(id: id, imageUrl: imageURL, name: name, instruction: instruction)
    }
}
