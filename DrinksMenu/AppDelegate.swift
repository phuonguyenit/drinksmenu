//
//  AppDelegate.swift
//  DrinksMenu
//
//  Created by phuonguyen on 02/08/2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let drinkUseCase = DrinkUseCaseProvider.shared.makeDrinkUseCase()
        let viewModel = DrinksMenuViewModel(useCase: drinkUseCase)
        let viewController = DrinksMenuViewController()
        viewController.viewModel = viewModel
        window?.rootViewController = viewController
        
        window?.makeKeyAndVisible()
        return true
    }
}

