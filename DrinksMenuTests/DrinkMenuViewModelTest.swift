//
//  DrinkMenuViewModelTest.swift
//  DrinksMenuTests
//
//  Created by phuonguyen on 05/08/2022.
//

import Foundation
import XCTest
import RxSwift
import RxCocoa
import RxTest

@testable import DrinksMenu

class DrinksMenuViewModelTest: XCTestCase {
    var viewModel: DrinksMenuViewModel!
    
    override func setUp() {
        super.setUp()
        let drinkUseCase = MockDataGenerator.shared.mockDrinkUseCase()
        viewModel = DrinksMenuViewModel(useCase: drinkUseCase)
    }
    
    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }
    
    override func setUpWithError() throws {
        
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
}
