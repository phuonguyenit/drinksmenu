//
//  DrinkUseCase.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation
import RxSwift
import RxCocoa

protocol DrinkUseCaseType  {
    func fetchDrinksFromNetwork() -> Single<Drink>
    func fetchDrinksFromDatabase() -> Observable<[DrinkItem]>
    func saveDrinkItemToDatabase(_ item : DrinkItem) -> Observable<Void>
}

final class DrinkUseCase: DrinkUseCaseType {
    private let drinkNetworkService: DrinkNetworkType
    private let drinkRealmRepo: DrinkRealmRepoType
    
    init(drinkNetworkService: DrinkNetworkType, drinkRealmRepo: DrinkRealmRepoType) {
        self.drinkNetworkService = drinkNetworkService
        self.drinkRealmRepo = drinkRealmRepo
    }
    
    func fetchDrinksFromNetwork() -> Single<Drink> {
        return drinkNetworkService.fetchDrinks()
    }
    
    func fetchDrinksFromDatabase() -> Observable<[DrinkItem]> {
        return drinkRealmRepo.queryAll()
    }
    
    func saveDrinkItemToDatabase(_ item: DrinkItem) -> Observable<Void> {
        return drinkRealmRepo.save(item)
    }
}
