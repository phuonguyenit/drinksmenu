//
//  BaseRequest.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation

struct NetWorkError: Error {
    var message: String
}

protocol BaseRequestProtocol {
    associatedtype ResponseType: Decodable
    var path: String { get }
}
