//
//  DrinkItem.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation

struct DrinkItem: Decodable {
    var id: String
    var imageUrl: String?
    var name: String?
    var instruction: String?
    
    
    enum CodingKeys: String, CodingKey {
        case idDrink, strDrink, strInstructions, strDrinkThumb
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .idDrink) ?? ""
        name = try values.decodeIfPresent(String.self, forKey: .strDrink)
        instruction = try values.decodeIfPresent(String.self, forKey: .strInstructions)
        imageUrl = try values.decodeIfPresent(String.self, forKey: .strDrinkThumb)
    }
    
    init(id: String, imageUrl: String?, name: String?, instruction: String?) {
        self.id = id
        self.imageUrl = imageUrl
        self.name = name
        self.instruction = instruction
    }
}
