//
//  DrinkUseCaseTest.swift
//  DrinksMenuTests
//
//  Created by phuonguyen on 05/08/2022.
//

import Foundation
import RxCocoa
import RxSwift
import RxTest
import XCTest
import RxBlocking
@testable import DrinksMenu


class DrinkUseCaseTest: XCTestCase {
    
    var drinkUseCase: DrinkUseCaseType!
    var drinkNetwork: DrinkNetworkMock!
    var drinkRealmRepository: DrinkRealmRepositoryMock!

    override func setUpWithError() throws {
        drinkNetwork = DrinkNetworkMock()
        drinkRealmRepository = DrinkRealmRepositoryMock()
        drinkUseCase = DrinkUseCase(drinkNetworkService: drinkNetwork, drinkRealmRepo: drinkRealmRepository)
    }

    override func tearDownWithError() throws {

    }

    func test_fetchDrinksFromNetWork() {
        XCTAssertFalse(drinkNetwork.isFetchDrinksCalled)
        
        let result = drinkUseCase.fetchDrinksFromNetwork().asObservable()
        
        XCTAssertTrue(drinkNetwork.isFetchDrinksCalled)
        
        do {
            guard let drink = try result.toBlocking().first() else { return }
            XCTAssertEqual(drink, MockDataGenerator.shared.mockDrinkData())
        } catch {}
    }
    
    func test_fetchDrinksFromDatabase() {
        XCTAssertFalse(drinkRealmRepository.isQueryAllCalled)
        
        let result = drinkUseCase.fetchDrinksFromDatabase()
        XCTAssertTrue(drinkRealmRepository.isQueryAllCalled)
        
        do {
            guard let items = try result.toBlocking().first() else { return }
            XCTAssertEqual(items, MockDataGenerator.shared.mockDrinkItemsData())
        } catch {}
    }
    
    func test_saveDrinkItemToDatabase() {
        XCTAssertFalse(drinkRealmRepository.isSaveCalled)
        
        let drinkItem = MockDataGenerator.shared.mockDrinkItem()
        _ = drinkUseCase.saveDrinkItemToDatabase(drinkItem)
        XCTAssertTrue(drinkRealmRepository.isSaveCalled)
    }
}
