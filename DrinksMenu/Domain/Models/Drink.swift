//
//  Drink.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation

struct Drink: Decodable {
    var items: [DrinkItem]
    enum CodingKeys: String, CodingKey {
        case drinks
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        items = try values.decodeIfPresent([DrinkItem].self, forKey: .drinks) ?? []
    }
    
    init(_ items: [DrinkItem]) {
        self.items = items
    }
}
