//
//  RMDrinkItem.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation
import Realm
import RealmSwift

final class RMDrinkItem: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var imageUrl: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var instruction: String = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

extension RMDrinkItem: DomainConvertibleType {
    func asDomain() -> DrinkItem {
        return DrinkItem(
            id: id,
            imageUrl: imageUrl,
            name: name,
            instruction: instruction
        )
    }
}

extension DrinkItem : RealmRepresentable {
    func asRealm() -> RMDrinkItem {
        return RMDrinkItem.build { object in
            object.id = id
            object.imageUrl = imageUrl ?? ""
            object.name = name ?? ""
            object.instruction = instruction ?? ""
        }
    }
}
