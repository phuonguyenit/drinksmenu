//
//  ViewController.swift
//  DrinksMenu
//
//  Created by phuonguyen on 02/08/2022.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import EmptyDataSet_Swift
import MBProgressHUD

final class DrinksMenuViewController: UIViewController {
    var viewModel: DrinksMenuViewModel!
    private let reloadData = PublishRelay<Void>()
    private let disposeBag = DisposeBag()
    
    // MARK: - Views
    private let headerView: UIView = {
        let view = UIView()
        view.backgroundColor = .groupTableViewBackground
        return view
    }()
    
    private let titleLabel: UILabel = {
        let view = UILabel()
        view.text = "Drinks Menu"
        view.textColor = .black
        return view
    }()
    
    private let contentTableView: UITableView = {
        let view = UITableView()
        view.separatorStyle = .none
        view.showsVerticalScrollIndicator = false
        view.rowHeight = UITableView.automaticDimension
        view.backgroundColor = .groupTableViewBackground
        return view
    }()
    
    // MARK: - View life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .groupTableViewBackground
        setupViews()
        setupConstraints()
        bind()
    }
    
    // MARK: - Setup
    private func setupViews() {
        headerView.addSubview(titleLabel)
        view.addSubview(headerView)
        view.addSubview(contentTableView)
        
        configureTableView()
    }
    
    private func setupConstraints() {
        headerView.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(44)
        }
        
        titleLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        contentTableView.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func configureTableView() {
        contentTableView.refreshControl = UIRefreshControl()
        contentTableView.register(DrinksMenuCell.self, forCellReuseIdentifier: DrinksMenuCell.reuseID)
        contentTableView.rx.setDelegate(self).disposed(by: disposeBag)
        contentTableView.emptyDataSetSource = self
        contentTableView.emptyDataSetDelegate = self
    }
    
    private func bind() {
        let refreshTrigger = contentTableView.refreshControl?.rx.controlEvent(.valueChanged).asDriver().mapToVoid() ?? Driver.empty()
        
        let loadDrinksTrigger = Driver.merge([Driver.just(()), reloadData.asDriverOnErrorJustComplete(), refreshTrigger])
        let input = DrinksMenuViewModel.Input(loadDrinksTrigger: loadDrinksTrigger)
        let output = viewModel.transform(input: input)
        output.outDrinks
            .drive(
                contentTableView.rx.items(
                    cellIdentifier: DrinksMenuCell.reuseID,
                    cellType: DrinksMenuCell.self
                )
            ) { index, item, cell in
                cell.bind(item)
            }
            .disposed(by: disposeBag)
        
        output.isLoading
            .drive(onNext: { [weak self] isLoading in
                self?.controlShowProgressLoading(isLoading)
                if !isLoading {
                    self?.contentTableView.refreshControl?.endRefreshing()
                }
            })
            .disposed(by: disposeBag)
        
        output.error
            .drive(onNext: { error in
                // TODO: show error
            })
            .disposed(by: disposeBag)
    }
    
    private func controlShowProgressLoading(_ isLoading: Bool) {
        if isLoading {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
}

// MARK: - UITableViewDelegate
extension DrinksMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

// MARK: - EmptyDataSource
extension DrinksMenuViewController: EmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return NSAttributedString(string: "Empty Data")
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return NSAttributedString(string: "Please make sure your internet connected!")
    }
    
    func buttonImage(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> UIImage? {
        return UIImage(named: "backgroundButton")
    }
}

// MARK: - EmptyDataSetDelegate
extension DrinksMenuViewController: EmptyDataSetDelegate {
    func emptyDataSet(_ scrollView: UIScrollView, didTapButton button: UIButton) {
        reloadData.accept(())
    }
}
