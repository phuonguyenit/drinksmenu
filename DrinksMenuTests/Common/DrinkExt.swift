//
//  DrinkExt.swift
//  DrinksMenuTests
//
//  Created by phuonguyen on 05/08/2022.
//

import Foundation
@testable import DrinksMenu

extension Drink: Equatable {
    public static func == (lhs: Drink, rhs: Drink) -> Bool {
        return lhs.items == rhs.items
    }
}

extension DrinkItem: Equatable {
    public static func == (lhs: DrinkItem, rhs: DrinkItem) -> Bool {
        return lhs.id == rhs.id
    }
}
