//
//  DrinkRealmRepositoryMock.swift
//  DrinksMenuTests
//
//  Created by phuonguyen on 05/08/2022.
//

import Foundation
import RxCocoa
import RxSwift
@testable import DrinksMenu

final class DrinkRealmRepositoryMock: DrinkRealmRepoType {
    
    var isQueryAllCalled = false
    func queryAll() -> Observable<[DrinkItem]> {
        isQueryAllCalled = true
        let drinkItems = MockDataGenerator.shared.mockDrinkItemsData()
        return Observable.just(drinkItems)
    }
    
    var isSaveCalled = false
    func save(_ item: DrinkItem) -> Observable<Void> {
        isSaveCalled = true
        return Observable.just(())
    }
}
