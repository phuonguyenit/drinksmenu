//
//  NetworkProvider.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation

final class NetworkFactory {
    static let shared = NetworkFactory()
    
    private let apiEndpoint: String
    public init() {
        apiEndpoint = "https://www.thecocktaildb.com/api/json"
    }

    public func makeDrinkNetwork() -> DrinkNetworkType {
        let network = Network(apiEndpoint)
        return DrinkNetwork(network: network)
    }
}
