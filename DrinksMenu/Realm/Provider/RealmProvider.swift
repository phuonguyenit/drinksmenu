//
//  RealmProvider.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation
import Realm
import RealmSwift

final class RealmProvider {
    static let shared = RealmProvider()
    
    private let configuration: Realm.Configuration

    public init(configuration: Realm.Configuration = Realm.Configuration()) {
        self.configuration = configuration
    }
    
    public func makeDinkrepo() -> DrinkRealmRepoType {
        let repository = RealmRepository<DrinkItem>(configuration: configuration)
        let drinkRepo = DrinkRealmRepository(repository: repository)
        return drinkRepo
    }
}
