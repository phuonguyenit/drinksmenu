//
//  DrinkUseCaseProvider.swift
//  DrinksMenu
//
//  Created by phuonguyen on 05/08/2022.
//

import Foundation

final class DrinkUseCaseProvider {
    static let shared = DrinkUseCaseProvider()
    
    private let drinkNetworkService = NetworkFactory.shared.makeDrinkNetwork()
    private let drinkRealmRepo = RealmProvider.shared.makeDinkrepo()
    
    func makeDrinkUseCase() -> DrinkUseCaseType {
        return DrinkUseCase(drinkNetworkService: drinkNetworkService, drinkRealmRepo: drinkRealmRepo)
    }
}
