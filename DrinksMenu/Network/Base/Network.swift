//
//  Network.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift

final class Network {
    private let endPoint: String
    private let scheduler: ConcurrentDispatchQueueScheduler
    
    init(_ endPoint: String) {
        self.endPoint = endPoint
        self.scheduler = ConcurrentDispatchQueueScheduler(qos: DispatchQoS(qosClass: DispatchQoS.QoSClass.background, relativePriority: 1))
    }
    
    func excute<T: BaseRequestProtocol>(_ request: T) -> Single<T.ResponseType> {
        let absolutePath = "\(endPoint)\(request.path)"
        return RxAlamofire
            .data(.get, absolutePath)
            .subscribe(on: scheduler)
            .asSingle()
            .flatMap { data in
                do {
                    let res = try JSONDecoder().decode(T.ResponseType.self, from: data)
                    return Single.just(res)
                } catch (let error) {
                    return Single.error(error)
                }
            }
            .debug()
    }
}
