import Foundation

protocol RealmRepresentable {
    associatedtype RealmType: DomainConvertibleType

    func asRealm() -> RealmType
}
