//
//  NetworkFactoryExtension.swift
//  DrinksMenuTests
//
//  Created by phuonguyen on 05/08/2022.
//

import Foundation
import RxCocoa
import RxSwift
import RxTest
@testable import DrinksMenu

extension NetworkFactory {
    
    func makeDrinkNetworkMock() -> DrinkNetworkType {
        return DrinkNetworkMock()
    }
}
