# DrinksMenu

## # Architecture

App use:

-   MVVM + Clean architecture

-   Reactive programming paradigm (RxSwift)

-   Cocoapods

## # Features
- [x] Loading list of drinks menu from API.
- [x] Support offline, save & cache data as a fallback, use Realm as a database.
- [x] Include Unit test.




## Installation

To make it easy for you to get started, flow this steps.

```
cd existing_dir
git clone https://gitlab.com/phuonguyenit/drinksmenu.git drinksmenu
cd drinksmenu
pod install
```
Now it's ready to open and run 

## Data source
1. API: https://www.thecocktaildb.com/api/json/v1/1/search.php?s=margarita
2. Design: https://www.figma.com/file/BEOvnjadqQSU0iaX1nZWQG/Drinks-Menu?node-id=0%3A1

###### :seedling: Clear mind clean code :trophy: Happy coding :tada:
