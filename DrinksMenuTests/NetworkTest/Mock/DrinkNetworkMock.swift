//
//  DrinkNetworkMock.swift
//  DrinksMenuTests
//
//  Created by phuonguyen on 05/08/2022.
//

import Foundation
import RxCocoa
import RxSwift
import RxTest
@testable import DrinksMenu

class DrinkNetworkMock: DrinkNetworkType {
    
    var isFetchDrinksCalled = false
    func fetchDrinks() -> Single<Drink> {
        isFetchDrinksCalled = true
        let drinkMock = MockDataGenerator.shared.mockDrinkData()
        return Single.just(drinkMock)
    }
}
