//
//  RealmRepository.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation
import Realm
import RealmSwift
import RxSwift
import RxRealm

protocol RealmRepoAbstract {
    associatedtype T
    func queryAll() -> Observable<[T]>
    func save(entity: T) -> Observable<Void>
}

final class RealmRepository<T:RealmRepresentable>: RealmRepoAbstract where T == T.RealmType.DomainType, T.RealmType: Object  {
    private let configuration: Realm.Configuration
    private let scheduler: RunLoopThreadScheduler

    private var realm: Realm {
        return try! Realm(configuration: self.configuration)
    }
    
    init(configuration: Realm.Configuration) {
        self.configuration = configuration
        let name = "Realm.Repository"
        self.scheduler = RunLoopThreadScheduler(threadName: name)
        print("File url: \(RLMRealmPathForFile("default.realm"))")
    }
    
    func queryAll() -> Observable<[T]> {
        return Observable.deferred {
            let realm = self.realm
            let objects = realm.objects(T.RealmType.self)
            
            return Observable.array(from: objects)
                .mapToDomain()
        }
        .subscribe(on: scheduler)
    }
    
    func save(entity: T) -> Observable<Void> {
        return Observable.deferred {
            return self.realm.rx.save(entity: entity)
        }
        .subscribe(on: scheduler)
    }
}
