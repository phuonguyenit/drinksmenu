//
//  ViewModelType.swift
//  DrinksMenu
//
//  Created by phuonguyen on 04/08/2022.
//

import Foundation

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    func transform(input: Input) -> Output
}
